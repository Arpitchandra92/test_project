﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;


namespace EON_Code.Models
{
    public class clsRow
    {        
        public string Name { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public string Date { get; set; }
        public string SelectedDays { get; set; }
        public string AddReq { get; set; }
    }
    public class clsTable
    {
        public clsTable()
        {
            Rows = new List<clsRow>();
        }
        public List<clsRow> Rows { get; set; }
    }
    public class UserDataModel
    {
        public string Name;
        public string Email;
        public string Gender;
        public string SelectedDays;
        public string AddReq;
        public DateTime Date;

        public int SaveDetails()
        {
            
            SqlConnection con = new SqlConnection(GetConString.ConString());
            string query ="INSERT INTO User_final(Name, Email, Gender, Date, SelectedDays, AddReq)VALUES('" + Name + "', '"+ Email + "', '"+ Gender + "', '" + Date + "', '" + SelectedDays + "', '" + AddReq + "')";
            //"INSERT INTO User_Test (Name,Email,Addreq) values ('" + Name + "','" + Email + "','" + AddReq + "')";
            SqlCommand cmd = new SqlCommand(query, con);
            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();
            return i;
            
        }

        public List<clsRow> getDetails()
        {
            clsTable oTable = new clsTable();            
            SqlConnection con = new SqlConnection(GetConString.ConString());
            string query = "select * from User_final";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            con.Open();
            SqlDataReader d = cmd.ExecuteReader();
            while (d.Read())
            {
                clsRow oRow = new clsRow();
                oRow.Name = d.GetValue(1).ToString();
                oRow.Email = d.GetValue(2).ToString();
                oRow.Gender = d.GetValue(3).ToString();
                oRow.Date = d.GetValue(4).ToString();
                oRow.SelectedDays = d.GetValue(5).ToString();
                oRow.AddReq = d.GetValue(6).ToString();
                oTable.Rows.Add(oRow);

            }
            con.Close();
            return oTable.Rows;

        }
    }
}
