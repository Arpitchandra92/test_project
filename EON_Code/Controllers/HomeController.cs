﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using EON_Code.Models;

namespace EON_Code.Controllers
{
    public class HomeController : Controller
    {

        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public IActionResult Profile()
        {
            return View();
        }
        [HttpPost]
        public IActionResult GetDetails()
        {
            bool returntoform = false;
            UserDataModel umodel = new UserDataModel();
            umodel.Name = HttpContext.Request.Form["txtName"].ToString();
            if(String.IsNullOrEmpty(umodel.Name))
            {
                ViewBag.ResultName = "Please Enter a name";
                returntoform = true;
            }
            umodel.Email = HttpContext.Request.Form["txtEmail"].ToString();
            if (String.IsNullOrEmpty(umodel.Email))
            {
                ViewBag.ResultEmail = "Please Enter an email";
                returntoform = true;
            }
            bool email = IsValidEmail(umodel.Email);
            if(!email)
            {
                ViewBag.ResultEmail = "Please Enter a valid email";
                returntoform = true;
            }
            bool IsValidEmail(string email)
            {
                try
                {
                    var addr = new System.Net.Mail.MailAddress(email);
                    return addr.Address == email;
                }
                catch
                {
                    return false;
                }
            }
            umodel.Gender = HttpContext.Request.Form["Gender"].ToString();
            if (String.IsNullOrEmpty(umodel.Name))
            {
                ViewBag.ResultGender = "Please select a gender";
                returntoform = true;
            }           
            var CheckBox1str = HttpContext.Request.Form["SelectedDays"].ToString();            
            var CheckBox2str = HttpContext.Request.Form["SelectedDays2"].ToString();
            var CheckBox3str = HttpContext.Request.Form["SelectedDays3"].ToString();
            if (String.IsNullOrEmpty(CheckBox1str)&& String.IsNullOrEmpty(CheckBox2str)&& String.IsNullOrEmpty(CheckBox3str))
            {
                ViewBag.ResultSelectedDays = "Please select a a day ";
                returntoform = true;
            }
            //Convert.ToInt32(HttpContext.Request.Form["txtAge"]);
            var array = new[] { CheckBox1str, CheckBox2str, CheckBox3str};
            string fullAddress = string.Join(",", array.Where(s => !string.IsNullOrEmpty(s)));
            umodel.SelectedDays = fullAddress;

            string iDate = HttpContext.Request.Form["txtDate"].ToString();                    
            try
            {
                DateTime oDate = Convert.ToDateTime(iDate);
                umodel.Date = oDate;
            }
            catch
            {
                ViewBag.ResultDate = "Please enter a valid date ";
                returntoform = true;
            }

            
           
            umodel.AddReq = HttpContext.Request.Form["txtAddReq"].ToString();
            
            
            
            
            if (returntoform == false)
            {
                int result = umodel.SaveDetails();
                if (result > 0)
                {
                    ViewBag.Result = "Data Saved Successfully";
                    return View("Index");
                }
                else
                {
                    ViewBag.Result = "Something Went Wrong";
                    return View("Privacy");
                }                
            }
            else
            {
                return View("Privacy");
            }   
        }
        [Route("api/[controller]")]
        [ApiController]
        public class NoteController : Controller
        {
            [HttpGet("")]
            public int List(string username)
            {
                return 45;
            }
        }
    }
}
