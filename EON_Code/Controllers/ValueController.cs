﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using EON_Code.Models;
using System;
using System.Data;

namespace EON_Code.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValueController : ControllerBase
    {
        private static UserDataModel users = new UserDataModel();
        [HttpGet]
        public ActionResult<List<clsRow>> Get()
        {
            return users.getDetails();

        }
    }
}